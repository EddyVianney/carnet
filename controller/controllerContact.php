<?php
session_start();
require_once ('../model/contactManager.php');
require_once('../model/form.php');


if(isset($_GET['id']))
{
	$id = $_GET['id'];
	$_SESSION['currentContactId'] = $id;
	$contactManager = new contactManager;
	$contact = $contactManager->getContactInfos($id);
	$_SESSION['firstname'] = $contact['prenom'] ;
	$_SESSION['secondname'] = $contact['nom'] ;
	$_SESSION['email'] = $contact['email'];
	$_SESSION['phone'] = $contact['telephone'];
	$_SESSION['city'] =  $contact['ville'];
	echo json_encode($contact);
	exit;
}

if($_GET['action'] == 'update')
{
	
	echo json_encode($_SESSION);
	exit;
}

if($_POST['action'] =='create')
{
	$form = new Form ;
	$contactManager = new contactManager;
	$result = $form->validateForm($_POST);

	if($result['success'])
	{ 	
		$exist = $contactManager->contact_exist($_POST['email']);
		if(!$exist)
		{
			$contactManager->addContact($_POST);
			$result['exist'] = false;
		}
		else
		{
			$result['exist'] = true;
			$result['success'] = false;
		}
	}
	echo json_encode($result);
	exit;
}
else
{
	$result['success'] = false ;
	echo json_encode($result);
	exit;
}
?>
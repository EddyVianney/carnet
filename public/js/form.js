$(function(){
      
      $("inputp").focus(function(){

            var id = $(this).attr('id');

            $("#" + id + " + .message").fadeOut(500);
      });

      $("#create").click(function(e){

         e.preventDefault();
         $("#contact-infos").hide();
         $(".form-control").attr("value","");
         $("#action").attr("value","create");
         $("button").text("add contact");
         $(".message").empty();
         $("#addContact").slideDown();

      });

       $("#edit").click(function(e){
         e.preventDefault();
         var url = $(this).attr('href');
         $("#contact-infos").hide();
         $(".message").empty();
         $("#action").attr("value","update");
         $.ajax({

            type: 'GET',
            url: url,
            data : {},
            dataType : 'json',
            success: function(result){

               if(Object.keys(result).length)
               {
                  $("#firstname").attr("value",result.firstname);
                  $("#secondname").attr("value",result.secondname);
                  $("#email").attr("value",result.email);
                  $("#phone").attr("value",result.phone);
                  $("#city").attr("value",result.city);
                  $("button").text("update contact");
                  $("#addContact").slideDown();

               }
               else
               {  
                  alert("Sorry, you cannot edit !");
                 
               }
            }

         });

       });

      $(".contactim").click(function(e){

         e.preventDefault();
         $('a.active').removeClass("active");
         $(this).addClass("active");
         $(".info-message").hide();
         var url = $(this).attr('href');

         $.ajax({

            type: 'GET',
            url: url,
            data : {},
            dataType : 'json',
            success: function(result){

               if(Object.keys(result).length)
               {
                   var content = '\
                   <li class="list-group-item"><span class="key">Nom</span><span class="value">'+result.nom+'</span></li>\
                   <li class="list-group-item"><span class="key">Prenom</span><span class="value">'+result.prenom+'</span></li>\
                   <li class="list-group-item"><span class="key">E-mail</span><span class="value">'+result.email+'</span></li>\
                   <li class="list-group-item"><span class="key">Téléphone</span><span class="value">'+result.telephone+'</span></li>\
                   <li class="list-group-item"><span class="key">Ville, CP</span><span class="value">'+result.ville+'</span></li>';
                    $("#addContact").hide();
                    $("#contact-infos").show();
                    $("#contact-infos li:not(.active)").remove();
                    $("#contact-infos .active").after(content);
               }
               else
               {  
   
                  alert("Oups ! A problem has occured !");
               }
            }

         });


      });
      
      $("#addContact").submit(function(e){

         e.preventDefault();
         var postdata = $("#addContact").serialize();
         var url = $(this).attr('action');

         $.ajax({

            type: 'POST',
            url: url,
            data : postdata,
            dataType : 'json',
            success: function(result){

               if(result.success)
               {
                    $("#form-message").empty().append("new contact successfully added !");
                    $("#addContact")[0].reset();
               }
               else
               {  
                    if(result.exist)
                    {
                      alert("Sorry, this contact already exists !");
                    }
                    else
                    {
                       $("#firstname+ .message").html(result.errors.firstname).addClass("input-error");
                       $("#secondname+ .message").html(result.errors.secondname).addClass("input-error");
                       $("#email+ .message").html(result.errors.email).addClass("input-error");
                       $("#phone+ .message").html(result.errors.phone).addClass("input-error");
                       $("#city+ .message").html(result.errors.city).addClass("input-error");
                    }

               }
            }

         });

      });

})
<?php

class Form
{

	private static $errors = array();
	
	public function getErrors()
	{
		return self::$errors;
	}

	private function setError($field, $message)
	{
		self::$errors[$field] = $message;
	}

	private function validateSecondName($secondName)
	{
		if(empty($secondName))
		{
			throw new Exception ("please, enter your second name");
		}
	}

	private function validateFirstName($firstName)
	{
		if(empty($firstName))
		{
			throw new Exception ("please, enter your first name!");
		}
	}
	
	private function validatePhoneNumber($phoneNumber)
	{
		if(empty($phoneNumber))
		{
			throw new Exception ("please, enter your  phone number!");
		}
		else
		{
			if(!preg_match("/^[0-9]{10}$/", $phoneNumber))
			{
				throw new Exception ("please, your phone number must have 10 digits !");
			}
		}
	}

	private  function validateEmail($email)
	{
		if(!empty($email))
		{
			if(!filter_var($email,FILTER_VALIDATE_EMAIL))
			{
				throw new Exception("please, enter a valid email address !");
			}
		}
		else
		{
			throw new Exception("please, enter an email address !");
		}
	}

	private function validateCity($city)
	{
		if(empty($city))
		{
			throw new Exception("please, enter a valid city !");
		}
	}

	public function validateForm($array)
	{
			$result = array();
			$result['success'] = true ;

			try{

				$this->validateFirstName($array['firstname']);
			}
			catch(Exception $e)
			{
				$this->setError("firstname",$e->getMessage());
			}


			try{

				$this->validateSecondName($array['secondname']);
			}
			catch(Exception $e)
			{
				$this->setError("secondname",$e->getMessage());
			}

			try{

				$this->validateEmail($array['email']);
			}
			catch(Exception $e)
			{
				$this->setError("email",$e->getMessage());
			}

			try{

				$this->validatePhoneNumber($array['phone']);
			}
			catch(Exception $e)
			{
				$this->setError("phone",$e->getMessage());
			}

			try{

				$this->validateCity($array['city']);
			}
			catch(Exception $e)
			{
				$this->setError("city",$e->getMessage());
			}

			$result['errors'] = $this->getErrors();

			if(count($result['errors']) > 0)
			{
				$result['success'] = false ;
			}

		return $result ;
	}	

}
?>
<?php

require_once('database.php');

class contactManager extends Database
{
    public function clean_input($input)
    {
       $input = trim($input);
	   $input = stripslashes($input);
	   $input = htmlspecialchars($input);
	   return $input;
    }

	public function getContactList()
	{
		$db = self::connect();
		$request = "SELECT * FROM contact";
		$contacts = $db->query($request);
		return $contacts;
	}

	public function contact_exist($email)
    {
      $db = self::connect();
      $request = "SELECT * FROM contact WHERE email = ? ";
      $statement = $db->prepare($request);
      $email = $this->clean_input($email);
      $statement->execute(array($email));

      if($statement->rowCount())
      {
         
        return true ;
      }

      return false ;
   }

	public function addContact($contact)
	{
		$db = self::connect();
		$request = "INSERT INTO contact (prenom, nom, email, telephone, ville) VALUES (?,?,?,?,?)";

		$statement = $db->prepare($request);

		$firstname 	= 	$this->clean_input(ucfirst($contact['firstname']));
		$secondname =   $this->clean_input(strtoupper($contact['secondname']));
		$email      =   $this->clean_input($contact['email']);
		$phone      =   $this->clean_input($contact['phone']);
		$city       =   $this->clean_input($contact['city']);

		$statement->execute(array($firstname, $secondname, $email, $phone, $city));
	}

	public function getContactInfos($id)
	{
		$db = self::connect();
		$request = "SELECT * FROM contact WHERE id = ?";
		$statement = $db->prepare($request);
		$statement->execute(array($id));
	    $contact = $statement->fetch(PDO::FETCH_ASSOC);

	    return $contact ;
	}
}
?>
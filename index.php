<?php
require_once('controller/controller.php');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf8" />
        <meta charset="utf-8">   
        <meta http-equiv="X-UA-Compatible" content="IE=edge">  
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link type="text/css" rel="stylesheet" href="public/css/style.css">
    </head>
    <body>
    	<div class="container">
    		<div class="wrapper row">
                <div class="col-md-offset-2 col-md-8">
    			<div class="contactc-list col-md-5">
                    <div class="list-group">
                        <li class="list-group-item active table-striped">Contacts<a id ="create" class="btn btn-primary btn-carnet" href="controller/controllerContact.php?action=create">Créer</a></li>
                        <?= contactList();?>
                    </div>
    			</div>
    			<div class="contact-view col-md-5">
    				<div class="row">
    					<div class="col-sm-12">
                            <ul id="contact-infos" class="list-group">
                                <li class="list-group-item active table-striped">Coordonnées<a id="edit" class="btn btn-primary btn-carnet" href="controller/controllerContact.php?action=update">Editer</a></li>
                            </ul>
    						<form  id="addContact" method="post" action="controller/controllerContact.php">
                                <input id="#action" type="hidden" name="action" value="create">
    							<div class="form-group">
    								<label class="control-label" for="firstname">First name</label>
    								<input  type="text" id="firstname" name="firstname" class="form-control" placeholder="enter your first name ...">
    								<p class="message"></p>
    							</div>
    							<div class="form-group">
    								<label class="control-label" for="secondname">Second name</label>
    								<input  type="text" id="secondname" name="secondname" class="form-control" placeholder="enter your second name ...">
    								<p class="message"></p>
    							</div>
    							<div class="form-group">
    								<label class="control-label" for="email">Email</label>
    								<input  type="text" id="email" name="email" class="form-control" placeholder="enter your email ...">
    								<p class="message"></p>
    							</div>
    							<div class="form-group">
    								<label class="control-label" for="phone">Phone number</label>
    								<input  type="text" id="phone" name="phone" class="form-control" placeholder="enter your phone number ...">
    								<p class="message"></p>
    							</div>
    							<div class="form-group">
    								<label class="control-label" for="city">City</label>
    								<input  type="text" id="city" name="city" class="form-control" placeholder="enter your city ...">
    								<p class="message"></p>
    							</div>
    							<button type="submit" id="submit" class="btn-block submit-btn btn btn-primary">Add contact</button>
                                <p id="form-message"></p>
    						</form>
    					</div>
    				</div>
    			</div>
                </div>
    		</div>
    	</div>
    	<script src="public/js/form.js" type="text/javascript"></script>
	</body>
</html>   
















